var notif = document.querySelector( '#promo' );
var close_notif = document.querySelector( '#promo .close' );
var nav = document.querySelector( 'main nav ul' );
var nav_burger = document.querySelector( '.burger a' );

close_notif.addEventListener( 'click', function() {

    notif.classList.add( 'hidden' );

} );

nav_burger.addEventListener( 'click', function() {

    if ( nav.classList.contains( 'hidden' ) ) {

        nav.classList.remove( 'hidden' );

    } else {

        nav.classList.add( 'hidden' );

    }

} );