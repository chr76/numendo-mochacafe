var closeThis = function( mybutton, tohide ) {

    var btn = document.querySelector( mybutton );
    var el = document.querySelector( tohide ).classList;
    btn.addEventListener( 'click', function() {

        if ( el.contains( 'hidden' ) ) {
            
            el.remove( 'hidden' );
            
        } else {

            el.add( 'hidden' );
            
        }
        
    } );
    
}

closeThis( '.burger a', 'main nav ul' );
closeThis( '#promo .close', '#promo' );